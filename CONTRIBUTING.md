Projet Framagit du site [GrenouilleMoi](https://grenouillemoi.fr)

---

## Comment contribuer?

Ce projet contient avant toute chose un fichier Html, ainsi que le fichier CSV `public/data/listesolutions.csv` qui contient les données qui sont affichées sur le site.

### Règles de contribution

Quelques règles tout de fois pour faciliter la vie concernant le listing CSV :
1. Si vous ajoutez une ou des lignes, merci de les rajouter à la fin
2. Merci de respecter le formalisme
3. Ce fichier a vocation de ne contenir que des solutions d’entreprise française
4. Merci de compléter tous les champs et non juste un ou deux
5. Vous pouvez toujours créer des issues à la place, mais cela est plutot réservé au dev/debug
6. Un commentaire qui justifie l’ajout, la modification, la suppression, sera grandement apprécié
7. Si vous souhaitez rajouter une colonne, merci à tout du moins de la rajouter en dernière colonne, et de rajouter un point virgule à chaque fin de ligne

### Workflow gitlab

1. créez un compte framagit.org (attendre la validation par les modérateurs)
2. créez une bifurcation de ce projet
3. créez une nouvelle branche à partir de la la branche `contributions`
4. poussez vos modifications
5. créez une requête de fusion via https://framagit.org/thomas.jardinet/frogme/-/merge_requests en prenant votre branche de travail comme source, et la branche `contributions` du projet original comme cible
